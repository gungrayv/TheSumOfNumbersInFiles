﻿namespace TheSumOfNumbersInFiles
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectFolderBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.countFilesLabel = new System.Windows.Forms.Label();
            this.countFilesTB = new System.Windows.Forms.TextBox();
            this.resultTB = new System.Windows.Forms.TextBox();
            this.resultLabel = new System.Windows.Forms.Label();
            this.progressBarFiles = new System.Windows.Forms.ProgressBar();
            this.folderPathTB = new System.Windows.Forms.TextBox();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.folderBD = new System.Windows.Forms.FolderBrowserDialog();
            this.SuspendLayout();
            // 
            // SelectFolderBtn
            // 
            this.SelectFolderBtn.Location = new System.Drawing.Point(12, 12);
            this.SelectFolderBtn.Name = "SelectFolderBtn";
            this.SelectFolderBtn.Size = new System.Drawing.Size(87, 23);
            this.SelectFolderBtn.TabIndex = 2;
            this.SelectFolderBtn.Text = "Select Folder";
            this.SelectFolderBtn.UseVisualStyleBackColor = true;
            this.SelectFolderBtn.Click += new System.EventHandler(this.SelectFolderBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(105, 44);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 16;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // countFilesLabel
            // 
            this.countFilesLabel.AutoSize = true;
            this.countFilesLabel.Location = new System.Drawing.Point(12, 76);
            this.countFilesLabel.Name = "countFilesLabel";
            this.countFilesLabel.Size = new System.Drawing.Size(56, 13);
            this.countFilesLabel.TabIndex = 15;
            this.countFilesLabel.Text = "Count files";
            // 
            // countFilesTB
            // 
            this.countFilesTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.countFilesTB.Enabled = false;
            this.countFilesTB.Location = new System.Drawing.Point(74, 73);
            this.countFilesTB.Name = "countFilesTB";
            this.countFilesTB.Size = new System.Drawing.Size(489, 20);
            this.countFilesTB.TabIndex = 14;
            // 
            // resultTB
            // 
            this.resultTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.resultTB.Enabled = false;
            this.resultTB.Location = new System.Drawing.Point(74, 98);
            this.resultTB.Name = "resultTB";
            this.resultTB.Size = new System.Drawing.Size(489, 20);
            this.resultTB.TabIndex = 13;
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Location = new System.Drawing.Point(12, 101);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(37, 13);
            this.resultLabel.TabIndex = 12;
            this.resultLabel.Text = "Result";
            // 
            // progressBarFiles
            // 
            this.progressBarFiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarFiles.Location = new System.Drawing.Point(186, 44);
            this.progressBarFiles.Name = "progressBarFiles";
            this.progressBarFiles.Size = new System.Drawing.Size(377, 23);
            this.progressBarFiles.TabIndex = 11;
            // 
            // folderPathTB
            // 
            this.folderPathTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.folderPathTB.Enabled = false;
            this.folderPathTB.Location = new System.Drawing.Point(105, 15);
            this.folderPathTB.Name = "folderPathTB";
            this.folderPathTB.Size = new System.Drawing.Size(458, 20);
            this.folderPathTB.TabIndex = 10;
            // 
            // LoadBtn
            // 
            this.LoadBtn.Location = new System.Drawing.Point(12, 44);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(87, 23);
            this.LoadBtn.TabIndex = 9;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 131);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.countFilesLabel);
            this.Controls.Add(this.countFilesTB);
            this.Controls.Add(this.resultTB);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.progressBarFiles);
            this.Controls.Add(this.folderPathTB);
            this.Controls.Add(this.LoadBtn);
            this.Controls.Add(this.SelectFolderBtn);
            this.Name = "MainForm";
            this.Text = "Сумматор чисел в файлах";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SelectFolderBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label countFilesLabel;
        private System.Windows.Forms.TextBox countFilesTB;
        private System.Windows.Forms.TextBox resultTB;
        private System.Windows.Forms.Label resultLabel;
        private System.Windows.Forms.ProgressBar progressBarFiles;
        private System.Windows.Forms.TextBox folderPathTB;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.FolderBrowserDialog folderBD;
    }
}

