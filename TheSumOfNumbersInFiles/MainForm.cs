﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace TheSumOfNumbersInFiles
{
    public partial class MainForm : Form
    {
        private string folderName;
        string[] allFiles;
        private long fullSumm;
        Thread th;
        CancellationTokenSource cts = new CancellationTokenSource();
        bool _isCalc = false;

        public MainForm()
        {
            InitializeComponent();
        }

        private void SelectFolderBtn_Click(object sender, EventArgs e)
        {
            if (!_isCalc)
            {
                DialogResult result = folderBD.ShowDialog();
                if (result == DialogResult.OK)
                {
                    folderName = folderBD.SelectedPath;
                    folderPathTB.Text = folderName;
                }
            }
        }

        private void LoadBtn_Click(object sender, EventArgs e)
        {
            if (!_isCalc)
            {
                try
                {
                    if (folderName != null)
                    {
                        SelectFolderBtn.Enabled = false;
                        LoadBtn.Enabled = false;
                        fullSumm = 0;
                        progressBarFiles.Value = 0;
                        resultTB.Text = String.Empty;
                        countFilesTB.Text = String.Empty;
                        cts = new CancellationTokenSource();
                        th = new Thread(readFiles);
                        th.Start();
                    } else
                    {
                        MessageBox.Show("Не указана папка.");
                        SelectFolderBtn.Enabled = true;
                        LoadBtn.Enabled = true;
                    }
                }
                catch (ObjectDisposedException ex)
                {
                    return;
                }
                catch (InvalidOperationException ex)
                {
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void readFiles()
        {
            try
            {
                Invoke((MethodInvoker)delegate
                {
                    countFilesTB.Text = "PLEASE WAIT, READING FILES!";
                });
                allFiles = Directory.GetFiles(folderName, "*.txt", SearchOption.AllDirectories);
                Invoke((MethodInvoker)delegate
                {
                    progressBarFiles.Maximum = allFiles.Length;
                    progressBarFiles.Value = 0;
                    countFilesTB.Text = allFiles.Length.ToString();
                });
                foreach (string file in allFiles)
                {
                    ThreadPool.QueueUserWorkItem(s =>
                    {
                        CancellationToken token = (CancellationToken)s;
                        if (token.IsCancellationRequested)
                            return;
                        summNumber(file);

                    }, cts.Token);
                }
            }
            catch (ThreadAbortException ex)
            {
                return;
            }
            catch (ThreadInterruptedException ex)
            {
                return;
            }
            catch (ObjectDisposedException ex)
            {
                return;
            }
            catch (InvalidOperationException ex)
            {
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void summNumber(string pathToFile)
        {
            long summNumbersFromFile = 0;
            using (StreamReader sr = new StreamReader(pathToFile))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] numbers = getNormalNumberStringArray(line.Split(' '));
                    foreach (string s in numbers.Where(x => !x.Equals(String.Empty)))
                    {
                        try
                        {
                            summNumbersFromFile += Convert.ToInt64(s);
                        }
                        catch (FormatException ex)
                        {
                            continue;
                        }
                        catch (OverflowException ex)
                        {
                            MessageBox.Show("Сумма чисел в файлах превышает допустимое число!");
                        }
                    }
                }
            }
            try
            {
                Invoke((MethodInvoker)delegate
                {
                    progressBarFiles.Value += 1;
                    fullSumm += summNumbersFromFile;
                    resultTB.Text = fullSumm.ToString();
                    if (progressBarFiles.Value == progressBarFiles.Maximum)
                    {
                        _isCalc = false;
                        SelectFolderBtn.Enabled = true;
                        LoadBtn.Enabled = true;
                    }
                });
            }
            catch (ObjectDisposedException ex)
            {
                return;
            }
            catch (InvalidOperationException ex)
            {
                return;
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (th != null)
                    th.Interrupt();
                cts.Cancel();
                _isCalc = false;
                Invoke((MethodInvoker)delegate
                {
                    SelectFolderBtn.Enabled = true;
                    LoadBtn.Enabled = true;
                });
            }
            catch (ObjectDisposedException ex)
            {
                return;
            }
            catch (InvalidOperationException ex)
            {
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string[] getNormalNumberStringArray(string[] StrArr)
        {
            string[] res, tmp = new string[StrArr.Length];
            int i = 0;
            foreach (string s in StrArr)
            {
                tmp[i++] = Regex.Replace(s, @"[^\-0-9]", " ");
            }
            res = getArrayNumberFormArrayWithSpace(tmp);
            return res;
        }

        private string[] getArrayNumberFormArrayWithSpace(string[] StrArr)
        {
            List<string> tmp = new List<string>();
            foreach (string s in StrArr)
            {
                tmp.AddRange(s.Split(' '));
            }
            return tmp.ToArray();
        }
    }
}
